#~ The MIT License (MIT)

#~ Copyright (c) 2015 Rise Software, Canada and Mike Wallace

#~ Permission is hereby granted, free of charge, to any person obtaining a copy
#~ of this software and associated documentation files (the "Software"), to deal
#~ in the Software without restriction, including without limitation the rights
#~ to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#~ copies of the Software, and to permit persons to whom the Software is
#~ furnished to do so, subject to the following conditions:

#~ The above copyright notice and this permission notice shall be included in
#~ all copies or substantial portions of the Software.

#~ THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#~ IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#~ FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#~ AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#~ LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#~ OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#~ THE SOFTWARE.


# INSTALLATION INSTRUCTIONS
# This is a gimp plug-in made with python. In order to use it you must copy it to your plug-in folder. This
# is usually found in your home directory in a hidden gimp directory: ~/.gimp-2.8/plug-ins
# Once copied, you must set the script as executable with the following command: chmod +x rs_save_android_icons.py
# Restart gimp and it will auto-detect the new plug-in.  You will see a new "Android" top menu


import gtk
import gobject
import os
import gimpui
import gimp
import gimpplugin
from gimpenums import *

pdb = gimp.pdb


# These are the user configurable settings
MAX_OUTPUT_IMAGE_WIDTH = 2999
MAX_OUTPUT_IMAGE_HEIGHT = 2999

LDPI_FOLDER	  = "drawable-ldpi"
MDPI_FOLDER    = "drawable-mdpi"
HDPI_FOLDER    = "drawable-hdpi"
XHDPI_FOLDER   = "drawable-large-xhdpi"
XXHDPI_FOLDER  = "drawable-xxhdpi"
XXXHDPI_FOLDER = "drawable-xxxhdpi"

CUSTOM_PREFIX = ""

DEFAULT_ROOT_FOLDER = "~"
DEFAULT_OUTPUT_IMAGE_TYPE = 0 #OutputImageType.custom_resource
DEFAULT_IMAGE_NAME = ""       #Leaving to empty will use the image name as the default
DEFAULT_CUSTOM_SIZE_BASE = 0
DEFAULT_KEEP_RATIO = True
DEFAULT_CUSTOM_WIDTH = 50
DEFAULT_CUSTOM_HEIGHT = 50
DEFAULT_SAVE_MODE_TYPE = 0
DEFAULT_INTERPOLATION_TYPE = 0


# End of user configurable settings.

# --------------------------------------------------------------------------------------------

# Do not change this
IMAGE_PARASITE_NAME = "rs_export_android_images"

def warning_dialog(parent, primary, secondary=None):
	dlg = gtk.MessageDialog(parent, gtk.DIALOG_DESTROY_WITH_PARENT, gtk.MESSAGE_WARNING, gtk.BUTTONS_CLOSE, primary)

	if secondary:
		dlg.format_secondary_text(secondary)

	dlg.run()
	dlg.destroy()


class InterpolationType(object):
	cubic = INTERPOLATION_CUBIC
	lanczos = INTERPOLATION_LANCZOS
	linear = INTERPOLATION_LINEAR
	none = INTERPOLATION_NONE

	listStore = gtk.ListStore(gobject.TYPE_INT, gobject.TYPE_STRING)
	listStore.append([cubic, "Cubic"])
	listStore.append([lanczos, "Lanczos"])
	listStore.append([linear, "Linear"])
	listStore.append([none, "None"])


class OutputImageDetails(object):
	width = 0
	height = 1
	folderName = 2
	fileNamePostfix = 3

	def __init__(self):
		self.name = ""
		self.tooltip = ""
		self.namePrefix = ""
		self.size_listStore = gtk.ListStore(
			gobject.TYPE_INT,        # Width
			gobject.TYPE_INT, 		# Height
			gobject.TYPE_STRING, 	# Folder Name
			gobject.TYPE_STRING		# File name postfix
			)


class OutputImageType(object):
	global LDPI_FOLDER
	global MDPI_FOLDER
	global HDPI_FOLDER
	global XHDPI_FOLDER
	global XXHDPI_FOLDER
	global xxxhdpiFolde

	custom_resource = 0
	launcher_icons = 1
	menu_icons = 2
	actionBar_icons = 3
	statusBar_icons = 4
	tab_icons_icons = 5
	dialog_icons = 6
	listView_icons = 7
	fab_button = 8

	ldpi  = 0
	mdpi  = 1
	hdpi  = 2
	xhdpi = 3
	xxhdpi = 4
	xxxhdpi = 5

	dpi = [0] * 6
	dpi[ldpi]  = 0.75
	dpi[mdpi]  = 1.0
	dpi[hdpi]  = 1.5
	dpi[xhdpi] = 2.0
	dpi[xxhdpi] = 3.0
	dpi[xxxhdpi] = 4.0

	listStore = gtk.ListStore(gobject.TYPE_INT, gobject.TYPE_STRING, gobject.TYPE_PYOBJECT)

	details = OutputImageDetails()
	details.name = "Launcher Icons"
	details.namePrefix = "ic_launcher"
	details.size_listStore.append([36, 36, LDPI_FOLDER, "_36x36"])
	details.size_listStore.append([48, 48, MDPI_FOLDER, "_48x48"])
	details.size_listStore.append([72, 72, HDPI_FOLDER, "_72x72"])
	details.size_listStore.append([96, 96, XHDPI_FOLDER, "_96x96"])
	details.size_listStore.append([144, 144, XXHDPI_FOLDER, "_144x144"])
	details.size_listStore.append([192, 192, XXXHDPI_FOLDER, "_192x192"])
	listStore.insert(launcher_icons, [launcher_icons, details.name, details])

	details2 = OutputImageDetails()
	details2.name = "Menu Icons"
	details2.namePrefix = "ic_menu"
	details2.size_listStore.append([36, 36, LDPI_FOLDER, "_36x36"])
	details2.size_listStore.append([48, 48, MDPI_FOLDER, "_48x48"])
	details2.size_listStore.append([72, 72, HDPI_FOLDER, "_72x72"])
	details2.size_listStore.append([96, 96, XHDPI_FOLDER, "_96x96"])
	details2.size_listStore.append([144, 144, XXHDPI_FOLDER, "_144x144"])
	details2.size_listStore.append([192, 192, XXXHDPI_FOLDER, "_192x192"])
	listStore.insert(menu_icons, [menu_icons, details2.name, details2])

	details3 = OutputImageDetails()
	details3.name = "ActionBar Icons"
	details3.namePrefix = "ic_menu"
	details3.size_listStore.append([18, 18, LDPI_FOLDER, "_18x18"])
	details3.size_listStore.append([24, 24, MDPI_FOLDER, "_24x24"])
	details3.size_listStore.append([36, 36, HDPI_FOLDER, "_36x36"])
	details3.size_listStore.append([48, 48, XHDPI_FOLDER, "_48x48"])
	details3.size_listStore.append([72, 72, XXHDPI_FOLDER, "_72x72"])
	details3.size_listStore.append([96, 96, XXXHDPI_FOLDER, "_96x96"])
	listStore.insert(actionBar_icons, [actionBar_icons, details3.name, details3])

	details4 = OutputImageDetails()
	details4.name = "StatusBar Icons"
	details4.namePrefix = "ic_stat_notify"
	details4.size_listStore.append([18, 18, LDPI_FOLDER, "_18x18"])
	details4.size_listStore.append([24, 24, MDPI_FOLDER, "_24x24"])
	details4.size_listStore.append([36, 36, HDPI_FOLDER, "_36x36"])
	details4.size_listStore.append([48, 48, XHDPI_FOLDER, "_48x48"])
	details4.size_listStore.append([72, 72, XXHDPI_FOLDER, "_72x72"])
	details4.size_listStore.append([96, 96, XXXHDPI_FOLDER, "_96x96"])
	listStore.insert(statusBar_icons, [statusBar_icons, details4.name, details4])

	details5 = OutputImageDetails()
	details5.name = "Tab Icons"
	details5.namePrefix = "ic_tab"
	details5.size_listStore.append([24, 24, LDPI_FOLDER, "_24x24"])
	details5.size_listStore.append([32, 32, MDPI_FOLDER, "_32x32"])
	details5.size_listStore.append([48, 48, HDPI_FOLDER, "_48x48"])
	details5.size_listStore.append([64, 64, XHDPI_FOLDER, "_64x64"])
	details5.size_listStore.append([96, 96, XXHDPI_FOLDER, "_96x96"])
	details5.size_listStore.append([128, 128, XXXHDPI_FOLDER, "_128x128"])
	listStore.insert(tab_icons_icons, [tab_icons_icons, details5.name, details5])

	details6 = OutputImageDetails()
	details6.name = "Dialog Icons"
	details6.namePrefix = "ic_dialog"
	details6.size_listStore.append([24, 24, LDPI_FOLDER, "_24x24"])
	details6.size_listStore.append([32, 32, MDPI_FOLDER, "_32x32"])
	details6.size_listStore.append([48, 48, HDPI_FOLDER, "_48x48"])
	details6.size_listStore.append([64, 64, XHDPI_FOLDER, "_64x64"])
	details6.size_listStore.append([96, 96, XXHDPI_FOLDER, "_96x96"])
	details6.size_listStore.append([128, 128, XXXHDPI_FOLDER, "_128x128"])
	listStore.insert(dialog_icons, [dialog_icons, details6.name, details6])

	details7 = OutputImageDetails()
	details7.name = "ListView Icons"
	details7.namePrefix = "ic"
	details7.size_listStore.append([24, 24, LDPI_FOLDER, "_24x24"])
	details7.size_listStore.append([32, 32, MDPI_FOLDER, "_32x32"])
	details7.size_listStore.append([48, 48, HDPI_FOLDER, "_48x48"])
	details7.size_listStore.append([64, 64, XHDPI_FOLDER, "_64x64"])
	details7.size_listStore.append([96, 96, XXHDPI_FOLDER, "_96x96"])
	details7.size_listStore.append([128, 128, XXXHDPI_FOLDER, "_128x128"])
	listStore.insert(listView_icons, [listView_icons, details7.name, details7])

	details8 = OutputImageDetails()
	details8.name = "Custom"
	details8.namePrefix = CUSTOM_PREFIX
	details8.size_listStore.append([1, 1, LDPI_FOLDER, "todo"])
	details8.size_listStore.append([1, 1, MDPI_FOLDER, "todo"])
	details8.size_listStore.append([1, 1, HDPI_FOLDER, "todo"])
	details8.size_listStore.append([1, 1, XHDPI_FOLDER, "todo"])
	details8.size_listStore.append([1, 1, XXHDPI_FOLDER, "todo"])
	details8.size_listStore.append([1, 1, XXXHDPI_FOLDER, "todo"])
	listStore.insert(custom_resource, [custom_resource, details8.name, details8])

	details9 = OutputImageDetails()
	details9.name = "FAB Button"
	details9.namePrefix = "ic"
	details9.size_listStore.append([24, 24, LDPI_FOLDER, "_24x24"])
	details9.size_listStore.append([32, 32, MDPI_FOLDER, "_32x32"])
	details9.size_listStore.append([48, 48, HDPI_FOLDER, "_48x48"])
	details9.size_listStore.append([64, 64, XHDPI_FOLDER, "_64x64"])
	details9.size_listStore.append([96, 96, XXHDPI_FOLDER, "_96x96"])
	details9.size_listStore.append([128, 128, XXXHDPI_FOLDER, "_128x128"])
	listStore.insert(fab_button, [fab_button, details9.name, details9])



class SaveModeType(object):
	saveToDrawableFolders = 0
	saveToRootFolder = 1

	listStore = gtk.ListStore(gobject.TYPE_INT, gobject.TYPE_STRING)
	listStore.append([saveToDrawableFolders, "Save to Drawable folders below Root folder"])
	listStore.append([saveToRootFolder, "Append resolution to file name"])


class ExportAndroidImagesWindow(gtk.Window):

	def __init__ (self, img, layer, *args):
		global DEFAULT_OUTPUT_IMAGE_TYPE
		global MAX_OUTPUT_IMAGE_WIDTH
		global MAX_OUTPUT_IMAGE_HEIGHT
		global DEFAULT_ROOT_FOLDER

		self.running = False
		self.img = img
		self.layer = layer
		win = gtk.Window.__init__(self, *args)
		self.set_border_width(10)
		self.set_title("Export Android Images by MikeWallaceDev")
		self.set_position(gtk.WIN_POS_CENTER)

		# Enable Escape key to quit application
		accelgroup = gtk.AccelGroup()
		key, modifier = gtk.accelerator_parse('Escape')
		accelgroup.connect_group(key, modifier, gtk.ACCEL_VISIBLE, gtk.main_quit)
		self.add_accel_group(accelgroup)

		# Main Values and Controls
		self.outputImageTypeCtrl = gtk.ComboBox(OutputImageType().listStore)
		self.outputImageNameCtrl = gtk.Entry()

		self.keepImageRatioCtrl = gtk.CheckButton("Keep image ratio")
		self.imageRatioValue = 1
		self.updateOtherValue = True

		self.useAndroidNamingConventionCtrl = gtk.CheckButton("Use Android naming convention")

		self.interpolationTypeCtrl = gtk.ComboBox(InterpolationType().listStore)
		self.saveModeCtrl = gtk.ComboBox(SaveModeType().listStore)
		self.fileChooserButton = gtk.FileChooserButton("Choose output folder", backend=None)
		self.imageSizesTable = gtk.Table(6, 3, False)
		self.outputImageDetails = None
		self.outputImageType = None
		self.customSizeBaseCtrl = gtk.ComboBox()

		self.adjustmentWidth = gtk.Adjustment(value=DEFAULT_CUSTOM_WIDTH, lower=1, upper= MAX_OUTPUT_IMAGE_WIDTH, step_incr=1, page_incr=0, page_size=0)
		self.hscaleWidth = gtk.HScale(self.adjustmentWidth)
		self.spinButtonWidth = gtk.SpinButton(self.adjustmentWidth, climb_rate=0.0, digits=0)

		self.adjustmentHeight = gtk.Adjustment(value=DEFAULT_CUSTOM_HEIGHT, lower=1, upper= MAX_OUTPUT_IMAGE_HEIGHT, step_incr=1, page_incr=0, page_size=0)
		self.hscaleHeight = gtk.HScale(self.adjustmentHeight)
		self.spinButtonHeight = gtk.SpinButton(self.adjustmentHeight, climb_rate=0.0, digits=0)

		self.inputImageCrtl = gimpui.ImageSelector()

		self.customSizesBox = gtk.VBox(False, 10)

		# PNG Values and Controls
		self.compressionAdjustment = gtk.Adjustment(value=9, lower=0, upper=9, step_incr=1, page_incr=0, page_size=0)
		self.useAdam7InterlacingCtrl = gtk.CheckButton("Use Adam7 interlacing")
		self.useAdam7InterlacingCtrl.set_active(FALSE)
		self.writebKGDCtrl = gtk.CheckButton("Write bKGD chunk")
		self.writebKGDCtrl.set_active(TRUE)
		self.writegAMACtrl = gtk.CheckButton("Write gAMA chunk")
		self.writegAMACtrl.set_active(FALSE)
		self.writeoFFsCtrl = gtk.CheckButton("Write oFFs chunk")
		self.writeoFFsCtrl.set_active(FALSE)
		self.writepHYsCtrl = gtk.CheckButton("Write pHYs chunk")
		self.writepHYsCtrl.set_active(TRUE)
		self.writetIMECtrl = gtk.CheckButton("Write tIME chunk")
		self.writetIMECtrl.set_active(TRUE)
		self.writeCommentCtrl = gtk.CheckButton("Write comment")
		self.writeCommentCtrl.set_active(TRUE)
		self.preserveColorOfTransparentPixelsCtrl = gtk.CheckButton("Preserve color of transparent pixels")
		self.preserveColorOfTransparentPixelsCtrl.set_active(TRUE)

		# Obey the window manager quit signal:
		self.connect("destroy", gtk.main_quit)

		vbox = gtk.VBox(spacing=10, homogeneous=False)
		vbox.show()

		# Make the UI
		notebook = gtk.Notebook()
		notebook.show()
		vbox.add(notebook)

		self.initMainTab(notebook)
		self.initPngTab(notebook)

		self.initControlButtons(vbox)

		self.add(vbox)

		self.show()

		# Make Connections after all the objects are created
		self.keepImageRatioCtrl.set_active(DEFAULT_KEEP_RATIO)
		self.useAndroidNamingConventionCtrl.set_active(FALSE)
		self.adjustmentWidth.connect("value_changed", self.onCustomWidthChanged)
		self.adjustmentHeight.connect("value_changed", self.onCustomHeightChanged)

		# Update all the data and widgets
		self.outputImageTypeCtrl.set_active(1)
		self.outputImageTypeCtrl.set_active(DEFAULT_OUTPUT_IMAGE_TYPE)
		self.setInputImage(self.img)

		return win

	def initControlButtons(self, vbox) :
		hbox = gtk.HBox(spacing=20)
		hbox.show()

		btn = gtk.Button("Help")
		btn.show()
		btn.connect("clicked", self.onHelpClicked)
		hbox.add(btn)

		btn = gtk.Button("Close")
		btn.show()
		btn.connect("clicked", self.onCloseClicked)
		hbox.add(btn)

		btn = gtk.Button("Generate")
		btn.show()
		btn.connect("clicked", self.onApplyClicked)
		btn.set_flags(gtk.CAN_DEFAULT)
		self.set_default(btn)
		hbox.add(btn)

		vbox.add(hbox)

	def initMainTab(self, notebook) :
		vbox = gtk.VBox(homogeneous=False, spacing=10)
		vbox.set_border_width(10)
		vbox.show()

		hbox = gtk.HBox(True, 10)
		hbox.show()

		label = gtk.Label("Input image")
		label.show()
		halign = gtk.Alignment(0, 1, 0, 0)
		halign.show()
		halign.add(label)
		hbox.pack_start(halign, False)

		self.inputImageCrtl.show()
		self.inputImageCrtl.set_active_image(self.img)
		self.inputImageCrtl.connect("changed", self.onInputImageChanged)
		hbox.add(self.inputImageCrtl)
		vbox.add(hbox)

		# Root folder
		hbox = gtk.HBox(True, 10)
		hbox.show()

		label = gtk.Label("Choose root folder")
		label.show()
		halign = gtk.Alignment(0, 1, 0, 0)
		halign.show()
		halign.add(label)
		hbox.pack_start(halign, False)

		self.fileChooserButton.set_action(gtk.FILE_CHOOSER_ACTION_SELECT_FOLDER)
		self.fileChooserButton.set_current_folder(DEFAULT_ROOT_FOLDER)
		self.fileChooserButton.connect("selection-changed", self.onFileChooserButtonChanged)
		self.fileChooserButton.show()

		hbox.add(self.fileChooserButton)
		vbox.add(hbox)


		# Outfile Name
		hbox = gtk.HBox(True, 10)
		hbox.show()

		label = gtk.Label("Resource Name")
		label.show()
		halign = gtk.Alignment(0, 1, 0, 0)
		halign.show()
		halign.add(label)
		hbox.pack_start(halign, False)

		self.outputImageNameCtrl.show()
		self.outputImageNameCtrl.connect("changed", self.onOutputImageNameCtrlActivate)

		hbox.add(self.outputImageNameCtrl)
		vbox.add(hbox)

		# Use android naming convention
		self.useAndroidNamingConventionCtrl.show()
		self.useAndroidNamingConventionCtrl.connect("toggled", self.onUseAndroidNamingConventionCtrlToggled)

		vbox.add(self.useAndroidNamingConventionCtrl)

		# Android icons type
		hbox = gtk.HBox(True, 10)
		hbox.show()

		label = gtk.Label("Android output image type")
		label.show()
		halign = gtk.Alignment(0, 1, 0, 0)
		halign.show()
		halign.add(label)
		hbox.pack_start(halign, False)

		self.outputImageTypeCtrl.show()
		cell = gtk.CellRendererText()
		self.outputImageTypeCtrl.pack_start(cell)
		self.outputImageTypeCtrl.connect("changed", self.onOutputImageTypeChanged)
		self.outputImageTypeCtrl.add_attribute(cell, 'text', 1)
		self.outputImageTypeCtrl.set_active(0)
		hbox.add(self.outputImageTypeCtrl)
		vbox.pack_start(hbox)

		# The custom sizes controls
		self.initCustomSizesPanel(vbox)

		# The sizes view
		self.initOutputSizesPanel(vbox)

		# Save mode
		hbox = gtk.HBox(True, 10)
		hbox.show()

		label = gtk.Label("Save mode")
		label.show()
		halign = gtk.Alignment(0, 1, 0, 0)
		halign.show()
		halign.add(label)
		hbox.pack_start(halign, False)

		self.saveModeCtrl.show()
		cell = gtk.CellRendererText()
		self.saveModeCtrl.pack_start(cell)
		self.saveModeCtrl.add_attribute(cell, 'text', 1)
		self.saveModeCtrl.connect("changed", self.onSaveModeCrtlChanged)
		self.saveModeCtrl.set_active(DEFAULT_SAVE_MODE_TYPE)
		hbox.add(self.saveModeCtrl)
		vbox.pack_start(hbox)

		# Interpolation Type
		hbox = gtk.HBox(True, 10)
		hbox.show()

		label = gtk.Label("Interpolation")
		label.show()
		halign = gtk.Alignment(0, 1, 0, 0)
		halign.show()
		halign.add(label)
		hbox.pack_start(halign, False)

		self.interpolationTypeCtrl.show()
		cell = gtk.CellRendererText()
		self.interpolationTypeCtrl.pack_start(cell)
		self.interpolationTypeCtrl.add_attribute(cell, 'text', 1)
		self.interpolationTypeCtrl.set_active(DEFAULT_INTERPOLATION_TYPE)

		hbox.add(self.interpolationTypeCtrl)
		vbox.add(hbox)

		label = gtk.Label("Main")
		label.show()
		notebook.append_page(vbox, label)


	def initCustomSizesPanel(self, vbox) :
		#~ This is the box that contains all the widgets for the custom size
		self.customSizesBox.show()

		#~ Size to use as a base for custom size
		hbox = gtk.HBox(True, 10)
		hbox.show()

		label = gtk.Label("Base size")
		label.show()
		halign = gtk.Alignment(0, 1, 0, 0)
		halign.show()
		halign.add(label)
		hbox.add(halign)

		self.customSizeBaseCtrl.show()
		cell = gtk.CellRendererText()
		self.customSizeBaseCtrl.pack_start(cell)
		self.customSizeBaseCtrl.add_attribute(cell, 'text', 2)
		self.customSizeBaseCtrl.connect("changed", self.oncustomSizeBaseChanged)

		hbox.add(self.customSizeBaseCtrl)
		self.customSizesBox.add(hbox)

		# Keep Image Ratio
		self.keepImageRatioCtrl.show()
		self.keepImageRatioCtrl.connect("toggled", self.onkeepImageRatioCtrlToggled)
		self.customSizesBox.add(self.keepImageRatioCtrl)

		# Custom Icon - width
		hbox = gtk.HBox(spacing=10, homogeneous = False)
		hbox = gtk.HBox(False, 10)
		hbox.show()

		label = gtk.Label("Custom width")
		label.show()
		label.expand = False
		halign = gtk.Alignment(0, 1, 0, 0)
		halign.show()
		halign.add(label)
		halign.expand = False
		hbox.pack_start(halign, False, True)

		self.hscaleWidth.set_digits(0)
		self.hscaleWidth.set_draw_value(TRUE)
		self.hscaleWidth.expand=True
		self.hscaleWidth.show()
		halign = gtk.Alignment(0, 1, 1, 1)
		halign.show()
		halign.add(self.hscaleWidth)
		halign.expand = True
		hbox.pack_end(halign, True)

		self.spinButtonWidth.show()
		self.spinButtonWidth.expand = False
		halign = gtk.Alignment(1, 1, 0, 0)
		halign.show()
		halign.add(self.spinButtonWidth)
		halign.expand = False
		hbox.pack_end(halign, False, False, 10)

		self.customSizesBox.add(hbox)

		# Custom Icon - mdpi height (48 0 99999 1 10 0 1)
		hbox = gtk.HBox(spacing=10, homogeneous=False)
		hbox.show()

		label = gtk.Label("Custom height")
		label.show()
		label.expand = False
		halign = gtk.Alignment(0, 1, 0, 0)
		halign.show()
		halign.add(label)
		halign.expand = False
		hbox.pack_start(halign, False, True)

		self.hscaleHeight.set_digits(0)
		self.hscaleHeight.set_draw_value(TRUE)
		self.hscaleHeight.expand=True
		self.hscaleHeight.show()
		halign = gtk.Alignment(0, 1, 1, 1)
		halign.show()
		halign.add(self.hscaleHeight)
		halign.expand = True
		hbox.pack_end(halign, True)

		self.spinButtonHeight.show()
		self.spinButtonHeight.expand=False
		halign = gtk.Alignment(1, 1, 0, 0)
		halign.show()
		halign.add(self.spinButtonHeight)
		hbox.pack_end(halign, False, False, 10)

		self.customSizesBox.add(hbox)

		vbox.add(self.customSizesBox)


	def initOutputSizesPanel(self, vbox) :
		# Table that shows the sizes of the images
		self.imageSizesTable.show()
		vbox.add(self.imageSizesTable)


	def initPngTab(self, notebook) :
		vbox = gtk.VBox(False, 5)
		vbox.set_border_width(10)
		vbox.show()

		label = gtk.Label("Initialize PNG settings")
		label.show()
		vbox.pack_start(label)

		# "Deflate Compression factor (0-9)"     '(9 0 9 1 10 0 0)
		hbox = gtk.HBox(homogeneous=False, spacing=10)
		hbox.show()

		label = gtk.Label("Deflate Compression factor (0-9)")
		label.show()
		hbox.pack_start(label, False)

		hscale = gtk.HScale(self.compressionAdjustment)
		hscale.set_digits(0)
		hscale.set_draw_value(TRUE)
		hscale.show()

		hbox.add(hscale)
		vbox.add(hbox)

		# Use Adam7 interlacing          default : FALSE
		self.useAdam7InterlacingCtrl.show()
		vbox.add(self.useAdam7InterlacingCtrl)

		# Write bKGD                   TRUE
		self.writebKGDCtrl.show()
		vbox.add(self.writebKGDCtrl)


		# gAMA                       FALSE
		self.writegAMACtrl.show()
		vbox.add(self.writegAMACtrl)

		# Write oFFs                       FALSE
		self.writeoFFsCtrl.show()
		vbox.add(self.writeoFFsCtrl)

		# Write pHYs chunk                  TRUE
		self.writepHYsCtrl.show()

		vbox.add(self.writepHYsCtrl)

		# Write tIME chunk                  TRUE
		self.writetIMECtrl.show()

		vbox.add(self.writetIMECtrl)

		# Write comment                  TRUE
		self.writeCommentCtrl.show()

		vbox.add(self.writeCommentCtrl)

		# Preserve color of transparent pixels  TRUE
		self.preserveColorOfTransparentPixelsCtrl.show()

		vbox.add(self.preserveColorOfTransparentPixelsCtrl)

		label = gtk.Label("PNG")
		label.show()

		notebook.append_page(vbox, label)

	def onInputImageChanged(self, combo):
		print "onInputImageChanged"
		self.setInputImage(combo.get_active_image())


	def setInputImage(self, image):
		print "setInputImage"

		pdb.gimp_image_undo_thaw(self.img)

		self.img = image

		pdb.gimp_image_undo_freeze(self.img)

		self.imageRatioValue = float (pdb.gimp_image_height(self.img)) / float (pdb.gimp_image_width(self.img) )
		self.adjustmentWidth.set_value(pdb.gimp_image_width(self.img))

		if self.img.name is not None:
			self.outputImageNameCtrl.set_text(self.img.name)
		else:
			self.outputImageNameCtrl.set_text(DEFAULT_IMAGE_NAME)

		self.loadImageSettingsIntoGUI()



	def onOutputImageTypeChanged(self, combo):
		print "onOutputImageTypeChanged"

		if None == self.outputImageDetails:
			return

		self.customSizeBaseCtrl.set_model(self.outputImageDetails.size_listStore)
		self.customSizeBaseCtrl.set_active(DEFAULT_CUSTOM_SIZE_BASE)

		if False == self.customSizeBaseCtrl.get_sensitive():
			self.calculateCustomSizes()
			self.updateVariables(False)

		if (None == self.outputImageType) or (self.outputImageType != OutputImageType.custom_resource) :
			self.customSizesBox.set_sensitive(False)

		else :
			self.customSizesBox.set_sensitive(True)


	def oncustomSizeBaseChanged(self, widget):
		print "oncustomSizeBaseChanged"
		self.calculateCustomSizes()
		self.updateVariables(False)


	def onUseAndroidNamingConventionCtrlToggled(self, widget):
		self.calculateCustomSizes()
		self.updateVariables(False)

	def onkeepImageRatioCtrlToggled(self, widget):
		newHeight = self.adjustmentWidth.get_value() * self.imageRatioValue

		self.adjustmentHeight.set_value(newHeight)



	def onSaveModeCrtlChanged(self, combo):
		self.updateVariables(False)


	def onFileChooserButtonChanged(self, widget):
		self.updateVariables(False)


	def onOutputImageNameCtrlActivate(self, widget):
		self.updateVariables(False)

	def onCustomWidthChanged(self, adjustmentWidth):
		print "onCustomWidthChanged"

		if self.keepImageRatioCtrl.get_active():
			if True == self.updateOtherValue:
				self.updateOtherValue = False

				newHeight = adjustmentWidth.get_value() * self.imageRatioValue

				self.adjustmentHeight.set_value(newHeight)
				self.updateOtherValue = True

		self.calculateCustomSizes()
		self.updateVariables(False)

	def onCustomHeightChanged(self, adjustmentHeight):
		print "onCustomHeightChanged"

		if self.keepImageRatioCtrl.get_active():
			if True == self.updateOtherValue:
				self.updateOtherValue = False

				newWidth = adjustmentHeight.get_value() / self.imageRatioValue

				self.adjustmentWidth.set_value(newWidth)
				self.updateOtherValue = True

		self.calculateCustomSizes()
		self.updateVariables(False)

	def onCloseClicked(self, widget) :
		gtk.main_quit()


	def onHelpClicked(self, widget) :
		gtk.main_quit()
		print "Quitting..."
		sys.stdout.flush()


	def onApplyClicked(self, widget) :
		self.doSave(widget)


	def updateImageSizesTable(self):
		print "updateImageSizesTable"

		for child in self.imageSizesTable.get_children():
			self.imageSizesTable.remove(child)

		label = gtk.Label("File name")
		label.show()
		self.imageSizesTable.attach(label, 0, 1, 0, 1)

		label = gtk.Label("Width")
		label.show()
		self.imageSizesTable.attach(label,1, 2, 0, 1)

		label = gtk.Label("Height")
		label.show()
		self.imageSizesTable.attach(label, 2, 3, 0, 1)

		if None == self.outputImageDetails:
			return

		y = 1
		for imageDetails in self.outputImageDetails.size_listStore:

			sizeX =  imageDetails[OutputImageDetails.width]
			sizeY =  imageDetails[OutputImageDetails.height]
			folder = imageDetails[OutputImageDetails.folderName]
			postfix = imageDetails[OutputImageDetails.fileNamePostfix]

			if None != self.rootFolder:
				if self.saveModeType == SaveModeType.saveToDrawableFolders :
					outputFileName = folder + "/" + self.outputFileName

				elif self.saveModeType == SaveModeType.saveToRootFolder:
					outputFileName = self.outputFileName + postfix

				else:
					print "BOOM!"
			else:
				outputFileName = "Please choose a root folder"

			label = gtk.Label(outputFileName)
			label.show()

			halign = gtk.Alignment(0, 1, 0, 0)
			halign.show()
			halign.add(label)

			self.imageSizesTable.attach(halign, 0, 1, y, y + 1)

			label = gtk.Label(sizeX)
			label.show()
			self.imageSizesTable.attach(label,1, 2, y, y + 1)

			label = gtk.Label(sizeY)
			label.show()
			self.imageSizesTable.attach(label, 2, 3, y, y + 1)

			y = y + 1


	def calculateCustomSizes(self) :
		tree_iter = self.outputImageTypeCtrl.get_active_iter()

		if tree_iter == None:
			print("Output Image Type not set.")
			return

		model = self.outputImageTypeCtrl.get_model()
		self.outputImageType = model[tree_iter][0]
		self.outputImageDetails = model[tree_iter][2]

		# Nothing to do if we are not in custom mode
		if self.outputImageType != OutputImageType.custom_resource :
			return

		customWidth = self.adjustmentWidth.get_value()
		customHeight = self.adjustmentHeight.get_value()

		n = self.customSizeBaseCtrl.get_active()
		modifier = OutputImageType.dpi[n]

		rowNb = 0
		for row in  self.outputImageDetails.size_listStore :
			adjustedWidth = int(customWidth * (OutputImageType.dpi[rowNb] / modifier))
			adjustedHeight = int(customHeight * (OutputImageType.dpi[rowNb] / modifier))

			row [OutputImageDetails.width] = adjustedWidth
			row [OutputImageDetails.height] = adjustedHeight
			row [OutputImageDetails.fileNamePostfix] = "_" + str(adjustedWidth) + "x" + str(adjustedHeight)

			rowNb = rowNb + 1


	def updateVariables(self, doValidate) :
		print "updateVariables"

		# Get the interpolation type and set it in the context
		tree_iter = self.interpolationTypeCtrl.get_active_iter()
		if tree_iter != None:
			model = self.interpolationTypeCtrl.get_model()
			self.interpolationName = model[tree_iter][1]
			self.InterpolationType = model[tree_iter][0]
			#~ print("Interpolation : name=%s   value=%d" % (self.interpolationName, self.InterpolationType))
		else:
			print("Interpolation not set.")

		# Get the Save Mode
		tree_iter = self.saveModeCtrl.get_active_iter()
		if tree_iter != None:
			model = self.saveModeCtrl.get_model()
			self.saveModeName = model[tree_iter][1]
			self.saveModeType = model[tree_iter][0]
			# print("Save Mode = %s" % self.saveModeName)
		else:
			print("Save Mode not set.")

		# Get Output Image type (Launcher icons, menu icons, etc...)
		tree_iter = self.outputImageTypeCtrl.get_active_iter()
		if tree_iter != None:
			model = self.outputImageTypeCtrl.get_model()
			self.outputImageType = model[tree_iter][0]
			self.outputImageTypeName = model[tree_iter][1]
			self.outputImageDetails = model[tree_iter][2]
			#~ print("outputImageTypeName = %s" % self.outputImageTypeName)
		else:
			print("ERROR : Output Image Type not set.")

		# Get the root folder
		self.fileChooserButton
		self.rootFolder = self.fileChooserButton.get_filename()
		if doValidate:
			if None == self.rootFolder:
				warning_dialog(self, "You must select a root folder", secondary=None)
				return False

		# Add the prefix if required
		useAndroidNamingConvention = self.useAndroidNamingConventionCtrl.get_active()

		# Get the name for the output file
		tempOutputFileName = self.outputImageNameCtrl.get_text()

		if useAndroidNamingConvention and (self.outputImageDetails.namePrefix != ""):
			self.outputFileName = self.outputImageDetails.namePrefix

			if "" != tempOutputFileName :
				self.outputFileName +=  "_" + tempOutputFileName
		else :
			self.outputFileName = tempOutputFileName

		if doValidate :
			if self.outputFileName == "":
				warning_dialog(self, "You must select a Resource Name", secondary=None)
				return False


		self.updateImageSizesTable()


	def saveImageSettings(self):

		settings = '%s\n%s\n%d\n%d\n%d\n%d\n%d\n%d\n%d\n%d\n' % \
			(self.rootFolder,
			self.outputFileName,
			self.useAndroidNamingConventionCtrl.get_active(),
			self.outputImageNameCtrl.get_text(),
			self.customSizeBaseCtrl.get_active(),
			self.keepImageRatioCtrl.get_active(),
			self.adjustmentWidth.get_value(),
			self.adjustmentHeight.get_value(),
			self.saveModeType,
			self.interpolationTypeCtrl.get_active()
			)

		print "Saving parasite : ", settings
		para = self.img.attach_new_parasite(IMAGE_PARASITE_NAME, 0, settings)


	def readImageSettings(self):
		settings = self.img.parasite_find(IMAGE_PARASITE_NAME)
		DEBUG = False
		if settings and not DEBUG:
			print "Loading settings : ", settings

			settingsVals = map(lambda x: 0 if x == 'None' or x == '' else x, settings.data.split('\n'))
			rootFolder = settingsVals[0]
			outputFileName = settingsVals[1]
			useAndroidNamingConvention = int(settingsVals[2])
			outputImageType = int(settingsVals[3])
			customSizeBase = int(settingsVals[4])
			keepRatio = int(settingsVals[5])
			customWidth = int(settingsVals[6])
			customHeight = int(settingsVals[7])
			saveModeType = int(settingsVals[8])
			interpolationType = int(settingsVals[9])

			return rootFolder, outputFileName, useAndroidNamingConvention, outputImageType, customSizeBase, keepRatio, \
			customWidth, customHeight, saveModeType, interpolationType

		else:
			return DEFAULT_ROOT_FOLDER, \
			DEFAULT_IMAGE_NAME,         \
			FALSE,                      \
			DEFAULT_OUTPUT_IMAGE_TYPE,  \
			DEFAULT_CUSTOM_SIZE_BASE,   \
			DEFAULT_KEEP_RATIO,         \
			DEFAULT_CUSTOM_WIDTH,       \
			DEFAULT_CUSTOM_HEIGHT,      \
			DEFAULT_SAVE_MODE_TYPE,     \
			DEFAULT_INTERPOLATION_TYPE


	def loadImageSettingsIntoGUI(self):
		rootFolder, outputFileName, useAndroidNamingConvention, outputImageType, customSizeBase, keepRatio, \
		customWidth, customHeight, saveModeType, interpolationType = self.readImageSettings()

		self.fileChooserButton.set_current_folder(rootFolder)

		print "outputFileName = ", outputFileName
		if outputFileName is not "":
			self.outputImageNameCtrl.set_text(outputFileName)

		self.useAndroidNamingConventionCtrl.set_active(useAndroidNamingConvention)

		self.outputImageTypeCtrl.set_active(outputImageType)

		self.customSizeBaseCtrl.set_active(customSizeBase)

		self.keepImageRatioCtrl.set_active(keepRatio)

		self.adjustmentWidth.set_value(customWidth)
		self.adjustmentHeight.set_value(customHeight)

		self.saveModeCtrl.set_active(saveModeType)

		self.interpolationTypeCtrl.set_active(interpolationType)

		self.calculateCustomSizes()
		self.updateVariables(False)


	def doSave(self, widget) :
		if False == self.updateVariables(True):
			return

		pdb.gimp_context_set_interpolation(self.InterpolationType)

		imgCopy = self.img.duplicate()
		imgCopy.disable_undo()

		# flatten the copied image to a layer
		flattened_layer = pdb.gimp_image_merge_visible_layers(imgCopy, CLIP_TO_IMAGE)

		for imageDetails in self.outputImageDetails.size_listStore:
			sizeX =  imageDetails[OutputImageDetails.width]
			sizeY =  imageDetails[OutputImageDetails.height]
			folder = imageDetails[OutputImageDetails.folderName]
			postfix = imageDetails[OutputImageDetails.fileNamePostfix]

			#~ print folder, sizeX, sizeY, postfix

			if (sizeX < 1) or (sizeY < 1):
				warning_dialog(self, "Size error", "The width and height cannot be less than 1")
				return

			if self.saveModeType == SaveModeType.saveToDrawableFolders :
				outputDir = self.rootFolder + "/" + folder

				# Create the output folder if needed
				if not os.path.exists(outputDir):
					os.makedirs(outputDir)

				finalOutputFileName = outputDir + "/" + self.outputFileName

			elif self.saveModeType == SaveModeType.saveToRootFolder:
				finalOutputFileName = self.rootFolder + "/" + self.outputFileName + postfix
			else:
				print "BOOM!"

			# Create a new image of the correct size that will be used for the output
			outputImage =  pdb.gimp_image_new(sizeX, sizeY, RGB)

			# Create a new layer which is a copy of the flattened layer
			outputLayer = pdb.gimp_layer_new_from_drawable(flattened_layer, outputImage)

			# Add the output layer to the output image
			pdb.gimp_image_insert_layer(outputImage, outputLayer, None, -1)

			# Scale the output layer to the correct size, uses the context interpolation as defined above
			outputLayer.scale(sizeX, sizeY, True)

			print "saving file : " + finalOutputFileName

			# save the output image
			# tmp.layers[0].set_offsets(0, 0) # ADD THIS LINE  (is this needed? -MW2)
			pdb.file_png_save2(                # BEFORE THIS LINE
				# 1,
				outputImage,
				outputImage.layers[0],
				finalOutputFileName + ".png",
				finalOutputFileName + ".png",
				self.useAdam7InterlacingCtrl.get_active(),
				int(self.compressionAdjustment.get_value()),
				self.writebKGDCtrl.get_active(),
				self.writegAMACtrl.get_active(),
				self.writeoFFsCtrl.get_active(),
				self.writepHYsCtrl.get_active(),
				self.writetIMECtrl.get_active(),
				self.writeCommentCtrl.get_active(),
				self.preserveColorOfTransparentPixelsCtrl.get_active())

			pdb.gimp_image_delete(outputImage)

		pdb.gimp_image_delete(imgCopy)

		self.saveImageSettings()



#~  Is now a stand alone plug-in, does not use gimpfu.
class rs_export_android_images_plugin(gimpplugin.plugin):

	def query(self):
		gimp.install_procedure(
			"rs_export_android_images",
			"Resizes an image to all the needed Android sizes and exports it to the appropriate directory",
			"Help!!!",
			"+MikeWallaceDev <mike.wallace@risesoftware.com",
			"The MIT License (MIT) 2015",
			"2015",
			"<Image>/Android/Export Images...",  # <Toolbox>, <Image>, <Load>, or <Save>
			"*",
			PLUGIN,
			[( PDB_INT32, 'run_mode', "run mode"),(PDB_IMAGE, 'image', 'Image'), (PDB_DRAWABLE, 'drawable', 'Drawable')],
			[]);


	def rs_export_android_images(self, run_mode, img, layer) :
		gimp.context_push()
		pdb.gimp_image_undo_freeze(img)

		mainWindow = ExportAndroidImagesWindow(img, layer)
		gtk.main()

		pdb.gimp_image_undo_thaw(mainWindow.img)
		gimp.context_pop()

		print "Thank you, come again."

		return


if __name__ == '__main__':
	rs_export_android_images_plugin().start()

