#!/bin/bash

#~ The MIT License (MIT)

#~ Copyright (c) 2015 Rise Software, Canada and Mike Wallace

#~ Permission is hereby granted, free of charge, to any person obtaining a copy
#~ of this software and associated documentation files (the "Software"), to deal
#~ in the Software without restriction, including without limitation the rights
#~ to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#~ copies of the Software, and to permit persons to whom the Software is
#~ furnished to do so, subject to the following conditions:

#~ The above copyright notice and this permission notice shall be included in
#~ all copies or substantial portions of the Software.

#~ THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#~ IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#~ FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#~ AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#~ LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#~ OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#~ THE SOFTWARE.

# based on https://gist.github.com/deanet/3427090
#
# useful $HOME/.gdrive.conf options:
#    curl_args="--limit-rate 500K --progress-bar"


browser="Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:13.0) Gecko/20100101 Firefox/13.0.1"

destination_folder_id=${@: -1}
if expr "$destination_folder_id" : '^[A-Za-z0-9]\{28\}$' > /dev/null
then
    # all but last word
    set -- "${@:0:$#}"
else
    # upload to root
    unset destination_folder_id
fi

if [ -e $HOME/.gdrive.conf ]
then
    . $HOME/.gdrive.conf
fi

old_umask=`umask`
umask 0077

if [ -z "$username" ]
then
    read -p "username: " username
    unset token
    echo "username=$username" >> $HOME/.gdrive.conf
fi

if [ -z "$account_type" ]
then
    if expr "$username" : '^[^@]*$' > /dev/null || expr "$username" : '.*@gmail.com$' > /dev/null
    then
        account_type=GOOGLE
    else
        account_type=HOSTED
    fi
fi

if [ -z "$password$token" ]
then
    read -s -p "password: " password
    unset token
    echo
fi

if [ -z "$token" ]
then
    token=`curl --silent --data-urlencode Email=$username --data-urlencode Passwd="$password" --data accountType=$account_type --data service=writely --data source=cURL "https://www.google.com/accounts/ClientLogin" | sed -ne s/Auth=//p`
    sed -ie '/^token=/d' $HOME/.gdrive.conf
    echo "token=$token" >> $HOME/.gdrive.conf
fi
umask $old_umask

for file in "$@"
do
    slug=`basename "$file"`
    mime_type=`file --brief --mime-type "$file"`
    upload_link=`curl --silent --show-error --insecure --request POST --header "Content-Length: 0" --header "Authorization: GoogleLogin auth=${token}" --header "GData-Version: 3.0" --header "Content-Type: $mime_type" --header "Slug: $slug" "https://docs.google.com/feeds/upload/create-session/default/private/full${destination_folder_id+/folder:$destination_folder_id/contents}?convert=false" --dump-header - | sed -ne s/"Location: "//p`
    echo "$file:"
    curl --request POST --output /dev/null --data-binary "@$file" --header "Authorization: GoogleLogin auth=${token}" --header "GData-Version: 3.0" --header "Content-Type: $mime_type" --header "Slug: $slug" "$upload_link" $curl_args
done
