#!/bin/bash

#~ The MIT License (MIT)

#~ Copyright (c) 2015 Rise Software, Canada and Mike Wallace

#~ Permission is hereby granted, free of charge, to any person obtaining a copy
#~ of this software and associated documentation files (the "Software"), to deal
#~ in the Software without restriction, including without limitation the rights
#~ to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#~ copies of the Software, and to permit persons to whom the Software is
#~ furnished to do so, subject to the following conditions:

#~ The above copyright notice and this permission notice shall be included in
#~ all copies or substantial portions of the Software.

#~ THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#~ IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#~ FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#~ AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#~ LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#~ OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#~ THE SOFTWARE.

echo "This script is deprecated. Please use the Python version named wifiadb"

#Modify this with your IP range
MY_IP_RANGE="192\.168\.1"

#You usually shouldn't have to modify this
PORT_BASE=5555

###
### End of user configuranble variables.
###

#List the devices on the screen for your viewing pleasure
adb devices
echo

#Find USB devices only (no emulators, genymotion or connected devices
declare -a deviceArray=(`adb devices -l | grep -v emulator | grep -v vbox | grep -v "${MY_IP_RANGE}" | grep " device " | awk '{print $1}'`)


echo "found ${#deviceArray[@]} device(s)"
echo

for index in ${!deviceArray[*]}
do
    echo "finding IP address for device ${deviceArray[index]}"
    IP_ADDRESS=$(adb -s ${deviceArray[index]} shell ifconfig wlan0 | awk '{print $3}')

    echo "IP address found : $IP_ADDRESS "

    echo "Connecting..."
    adb -s ${deviceArray[index]} tcpip $(($PORT_BASE + $index))
    adb -s ${deviceArray[index]} connect "$IP_ADDRESS:$(($PORT_BASE + $index))"

    echo
    echo
done

adb devices -l
#exit
